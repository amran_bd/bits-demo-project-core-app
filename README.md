# BITS Demo Project Core APP

In this project only two things included: 1. User Login 2. Employee crud operation using token based rest API communication. An user 1st of generate token then communicate with application using access_token.

# Please follow this step:
TECHNOLOGY: Java-8, Spring Boot, Spring, Data, Spring Security, JPA
1. run script profile_mgt_by_amran.sql file into mysql database;
2.run demo project into Intlij IDE and packaging or run 8080 port.
3.8080 port is static define in UI Apllication that's why need localhost 8080 port.
4. Please don't deploy. If you deploy war file in tomcat please re-write constant file in the ui application. change the URL http://localhost:8080/demo
#If you call rest api from postman you should be follow bellow step. 
1. Client Secret Name: secure-client
2. Client Secret Name: password@123
3. Grant Type: password
4. Username: amran
5. password: amran

#EXAMPLE:
For Generate access_token:
http://localhost:8080/oauth/token?grant_type=password&username=amran&password=amran

For Geting Employee List:

http://localhost:8080/api/v1/emp/list?access_token=0f0656d2-c424-4c3f-9b49-c9fd5fcb6ad7