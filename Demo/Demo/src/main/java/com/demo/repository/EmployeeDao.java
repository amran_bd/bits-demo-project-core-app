package com.demo.repository;

import com.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Md. Amran Hossain
 */
public interface EmployeeDao extends JpaRepository<Employee, Long> {
}
