package com.demo.repository;

import com.demo.model.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Md. Amran Hossain
 */
public interface UserDao extends JpaRepository<ApplicationUser, Long> {

    ApplicationUser findByUsername(String username);
}
