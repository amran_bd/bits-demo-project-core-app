package com.demo.model;

import javax.persistence.*;
import java.util.List;
/**
 * @author Md. Amran Hossain
 */
@Entity
@Table(name = "tbl_app_user",uniqueConstraints = {@UniqueConstraint(columnNames={"username"})})
public class ApplicationUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String username;
    private String password;

    @OneToMany(fetch = FetchType.EAGER, cascade= CascadeType.ALL)
    private List<AppUserRole> roles;

    public ApplicationUser() {
    }

    public ApplicationUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public ApplicationUser(String username, String password, List<AppUserRole> roles) {
        this.username = username;
        this.password = password;
        this.setRoles(roles);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public List<AppUserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<AppUserRole> roles) {
        this.roles = roles;
    }
}
