package com.demo.model.oauth2;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Md. Amran Hossain
 */
@Entity
@Table(name = "oauth_access_token",uniqueConstraints = {@UniqueConstraint(columnNames={"authentication","authentication_id"})})
public class OauthAccessTokenEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "token_id")
    private String tokenId;
    @Lob
    private byte[] token;
    @Column(name = "authentication_id")
    private String authenticationId;
    @Column(name = "user_name", unique = true)
    @NotNull
    private String userName;
    @Column(name = "client_id")
    private String clientId;
    @Lob
    private byte[] authentication;
    @Column(name = "refresh_token")
    private String refreshToken;

    public OauthAccessTokenEntity() {
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    public String getAuthenticationId() {
        return authenticationId;
    }

    public void setAuthenticationId(String authenticationId) {
        this.authenticationId = authenticationId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public byte[] getAuthentication() {
        return authentication;
    }

    public void setAuthentication(byte[] authentication) {
        this.authentication = authentication;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
