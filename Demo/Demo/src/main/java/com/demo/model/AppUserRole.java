package com.demo.model;

import javax.persistence.*;
/**
 * @author Md. Amran Hossain
 */
@Entity
@Table(name = "tbl_app_user_role")
public class AppUserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String roleName;

    AppUserRole() {
    }

    public AppUserRole(String roleName) {
        this.setRoleName(roleName);
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
