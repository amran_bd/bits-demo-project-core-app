package com.demo.util;

import com.demo.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Md. Amran Hossain
 */
@Component
public class DataLoader {

    @Autowired
    UserDao userDao;

    @Autowired
    PasswordEncoder passwordEncoder;


    @EventListener
    public void dataInsert(ApplicationReadyEvent event) {
//        userRoleDao.save(new AppUserRole("ADMIN"));
//        userRoleDao.save(new AppUserRole("USER"));
//        userDao.save(new ApplicationUser("amran",passwordEncoder.encode("amran")));
        System.out.println("Data Insert Successful.");
    }
}
