package com.demo.common;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

/**
 * @author Md. Amran Hossain
 */
public class EmployeeAddRequest implements Serializable {

    private Long empId;
    private String empName;
    private String designation;
    private String gender;
    private Long dateOfBirth;
    private Integer age;
    private byte[] profilePictuer;
    private String note;

    public EmployeeAddRequest() {
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public byte[] getProfilePictuer() {
        return profilePictuer;
    }

    public void setProfilePictuer(byte[] profilePictuer) {
        this.profilePictuer = profilePictuer;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
