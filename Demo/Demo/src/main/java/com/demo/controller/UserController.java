package com.demo.controller;

import com.demo.common.UserLoginRequest;
import com.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Md. Amran Hossain
 */

@Controller
@RequestMapping(value = "/api/v1/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST,produces = "application/json")
    public ResponseEntity<?> userLogin(@RequestBody UserLoginRequest request) {
        LOGGER.info("WebFilterController::getWebFilterLogData() method call..");
        if(request.getUserName() !=null && request.getPassword() !=null){
            if(userService.loginUser(request.getUserName(),request.getPassword())){
                return new ResponseEntity<>("200", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("Bad Credentials!", HttpStatus.BAD_REQUEST);
    }
}
