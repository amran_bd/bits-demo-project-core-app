package com.demo.controller;

import com.demo.common.EmployeeAddRequest;
import com.demo.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;

/**
 * @author Md. Amran Hossain | amrancse930@gmail.com
 */
@Controller
@RequestMapping(value = "/api/v1/emp")
public class EmployeeController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeService employeeService;

    public EmployeeController() {
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAllEmployees() {
        LOGGER.info("EmployeeController::getAllEmployees() method call..");
        return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> addEmployee(@RequestBody EmployeeAddRequest request) {
        LOGGER.info("EmployeeController::addEmployee() method call..");
        if(request !=null){
           if(employeeService.addEmployee(request)){
               return new ResponseEntity<>("200", HttpStatus.OK);
           }
        }
        return new ResponseEntity<>("400", HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> delteEmployee(@PathVariable Long empId) {
        LOGGER.info("EmployeeController::addEmployee() method call..");
        if(empId !=null){
            if(employeeService.deleteEmployee(empId)){
                return new ResponseEntity<>("200", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("400", HttpStatus.BAD_REQUEST);
    }
}
