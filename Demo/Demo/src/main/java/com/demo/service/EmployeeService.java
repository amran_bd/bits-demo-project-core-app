package com.demo.service;

import com.demo.common.EmployeeAddRequest;
import com.demo.model.Employee;

import java.util.List;

/**
 * @author Md. Amran Hossain
 */
public interface EmployeeService {

    List<Employee> getAllEmployees();

    Boolean addEmployee(EmployeeAddRequest addRequest);

    Boolean deleteEmployee(Long empId);
}
