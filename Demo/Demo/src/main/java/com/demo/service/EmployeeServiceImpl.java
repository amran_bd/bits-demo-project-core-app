package com.demo.service;

import com.demo.common.EmployeeAddRequest;
import com.demo.model.Employee;
import com.demo.repository.EmployeeDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Blob;
import java.util.Date;
import java.util.List;

/**
 * @author Md. Amran Hossain
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    EmployeeDao employeeDao;

    public EmployeeServiceImpl() {
    }


    @Override
    public List<Employee> getAllEmployees() {
        LOGGER.info("EmployeeServiceImpl::getAllEmployees() method call...");
        return employeeDao.findAll();
    }

    @Override
    public Boolean addEmployee(EmployeeAddRequest addRequest) {
        LOGGER.info("EmployeeServiceImpl::addEmployee() method call...");
        try {
            Employee employee = new Employee();
            employee.setEmpName(addRequest.getEmpName());
            employee.setDesignation(addRequest.getDesignation());
            employee.setGender(addRequest.getGender());
            employee.setDateOfBirth(new Date(addRequest.getDateOfBirth()));
            employee.setAge(addRequest.getAge());
            //employee.setProfilePictuer(addRequest.getProfilePictuer());
            employee.setNote(addRequest.getNote());
            Employee emp = employeeDao.save(employee);
            if(emp !=null) {
                return Boolean.TRUE;
            }
        }catch (Exception ex){
            LOGGER.info("EmployeeServiceImpl::addEmployee() method call..."+ex.getMessage());
        }
        return Boolean.FALSE;
    }

    @Override
    public Boolean deleteEmployee(Long empId) {
        LOGGER.info("EmployeeServiceImpl::deleteEmployee() method call...");
        try{
            Employee employee = employeeDao.getOne(empId);
            employeeDao.delete(employee);
            return Boolean.TRUE;
        }catch(Exception ex){
            LOGGER.info("EmployeeServiceImpl::deleteEmployee() method call..."+ex.getMessage());
        }
        return Boolean.FALSE;
    }
}
