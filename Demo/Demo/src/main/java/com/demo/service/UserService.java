package com.demo.service;

import com.demo.model.ApplicationUser;

import java.util.List;

/**
 * @author Md. Amran Hossain
 */
public interface UserService {

    List<ApplicationUser> findAll();

    Boolean loginUser(String userName, String password);
}
