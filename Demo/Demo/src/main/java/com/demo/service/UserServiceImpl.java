package com.demo.service;

import com.demo.model.ApplicationUser;
import com.demo.repository.UserDao;
import com.demo.security.AppUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Md. Amran Hossain
 */
@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserDetailsService, UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserDao userDao;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public List<ApplicationUser> findAll() {
        LOGGER.info("UserServiceImpl::findAll() method call...");
        return null;
    }

    @Override
    public Boolean loginUser(String userName, String password) {
        LOGGER.info("UserServiceImpl::loginUser() method call...");
        ApplicationUser applicationUser = userDao.findByUsername(userName);
        return passwordEncoder.matches(password,applicationUser.getPassword());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.info("UserServiceImpl::loadUserByUsername() method call...");
        ApplicationUser user = userDao.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new AppUserDetails(user);
    }
}
