package com.demo.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Md. Amran Hossain | Sr. Software Developer, HLC LLC
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
