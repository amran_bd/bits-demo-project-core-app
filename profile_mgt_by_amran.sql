-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.23-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema profile_mgt
--

CREATE DATABASE IF NOT EXISTS profile_mgt;
USE profile_mgt;

--
-- Definition of table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES 
 (18),
 (18),
 (18);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;


--
-- Definition of table `oauth_access_token`
--

DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE `oauth_access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authentication` longblob,
  `authentication_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `token` longblob,
  `token_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_1uk2c4t8599tl3uth0vg54is1` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_access_token`
--

--
-- Definition of table `oauth_approvals`
--

DROP TABLE IF EXISTS `oauth_approvals`;
CREATE TABLE `oauth_approvals` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(255) DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `last_modified_at` datetime DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_approvals`
--

/*!40000 ALTER TABLE `oauth_approvals` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_approvals` ENABLE KEYS */;


--
-- Definition of table `oauth_client_details`
--

DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL,
  `client_secret` longblob,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_client_details`
--

/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
INSERT INTO `oauth_client_details` (`client_id`,`access_token_validity`,`additional_information`,`authorities`,`authorized_grant_types`,`autoapprove`,`client_secret`,`refresh_token_validity`,`resource_ids`,`scope`,`web_server_redirect_uri`) VALUES 
 ('secure-client',2592000,NULL,'ROLE_CLIENT,ROLE_TRUSTED_CLIENT','password,refresh_token',NULL,0x7B6263727970747D243261243130245174497149304364317839726E4641543743356A72756E67366E43654E356A622E514F67666D6A443679716F6A4D684B4E754F7469,2592000,NULL,'read,write,trust',NULL);
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;


--
-- Definition of table `oauth_client_token`
--

DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE `oauth_client_token` (
  `authentication_id` varchar(255) NOT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `token` longblob,
  `token_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_client_token`
--

/*!40000 ALTER TABLE `oauth_client_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_token` ENABLE KEYS */;


--
-- Definition of table `oauth_code`
--

DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authentication` longblob,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_code`
--

/*!40000 ALTER TABLE `oauth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_code` ENABLE KEYS */;


--
-- Definition of table `oauth_refresh_token`
--

DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE `oauth_refresh_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authentication` longblob,
  `token` longblob,
  `token_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dyt1a9sjjp24o6ds2t7b5p3ae` (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_refresh_token`
--

--
-- Definition of table `tbl_app_user`
--

DROP TABLE IF EXISTS `tbl_app_user`;
CREATE TABLE `tbl_app_user` (
  `id` bigint(20) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKhyegr76adyv70gup95bd90wbn` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_app_user`
--

/*!40000 ALTER TABLE `tbl_app_user` DISABLE KEYS */;
INSERT INTO `tbl_app_user` (`id`,`password`,`username`) VALUES 
 (13,'{bcrypt}$2a$10$7X9oTWxB9QDw36FtslaGbu9qoYWN2cRA2BelgovXXlN8ySXKt7yD.','amran');
/*!40000 ALTER TABLE `tbl_app_user` ENABLE KEYS */;


--
-- Definition of table `tbl_app_user_role`
--

DROP TABLE IF EXISTS `tbl_app_user_role`;
CREATE TABLE `tbl_app_user_role` (
  `id` bigint(20) NOT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_app_user_role`
--

/*!40000 ALTER TABLE `tbl_app_user_role` DISABLE KEYS */;
INSERT INTO `tbl_app_user_role` (`id`,`role_name`) VALUES 
 (1,'ADMIN'),
 (2,'USER');
/*!40000 ALTER TABLE `tbl_app_user_role` ENABLE KEYS */;


--
-- Definition of table `tbl_app_user_roles`
--

DROP TABLE IF EXISTS `tbl_app_user_roles`;
CREATE TABLE `tbl_app_user_roles` (
  `application_user_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_gje5ieehjmopq3qjck7saq0rk` (`roles_id`),
  KEY `FKbp2pdic7qc3jecq7rblnxayrv` (`application_user_id`),
  CONSTRAINT `FKbp2pdic7qc3jecq7rblnxayrv` FOREIGN KEY (`application_user_id`) REFERENCES `tbl_app_user` (`id`),
  CONSTRAINT `FKq0bb27fiwaeqnnshgqldhwy4x` FOREIGN KEY (`roles_id`) REFERENCES `tbl_app_user_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_app_user_roles`
--

/*!40000 ALTER TABLE `tbl_app_user_roles` DISABLE KEYS */;
INSERT INTO `tbl_app_user_roles` (`application_user_id`,`roles_id`) VALUES 
 (13,1),
 (13,2);
/*!40000 ALTER TABLE `tbl_app_user_roles` ENABLE KEYS */;


--
-- Definition of table `tbl_employee_profile`
--

DROP TABLE IF EXISTS `tbl_employee_profile`;
CREATE TABLE `tbl_employee_profile` (
  `emp_id` bigint(20) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `emp_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `profile_pictuer` longblob,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee_profile`
--

/*!40000 ALTER TABLE `tbl_employee_profile` DISABLE KEYS */;
INSERT INTO `tbl_employee_profile` (`emp_id`,`age`,`date_of_birth`,`designation`,`emp_name`,`gender`,`note`,`profile_pictuer`) VALUES 
 (1,26,NULL,'TECH LEAD','Md. Amran Hossain','Male','gfhfh fhfhgtft',NULL),
 (15,27,'2019-03-31 00:00:00','Software Developer','Hossain','M','TTestttt',NULL),
 (16,23,'2019-03-22 00:00:00','TTT','Nahid','M','TTT',NULL),
 (17,0,'2019-03-22 00:00:00','TTT','Nahid','M','TTT',NULL);
/*!40000 ALTER TABLE `tbl_employee_profile` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
